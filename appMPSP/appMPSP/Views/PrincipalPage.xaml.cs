﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appMPSP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrincipalPage : MasterDetailPage
    {
        public PrincipalPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Abrindo tela de Contato
            MessagingCenter.Subscribe<HomePage>(this,"HomePageAbrir",
                    (sender) =>
                    {
                        this.Detail = new NavigationPage(new HomePage());
                        this.IsPresented = false;
                    });

            


        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<HomePage>(this, "HomePageAbrir");
        }


    }
}