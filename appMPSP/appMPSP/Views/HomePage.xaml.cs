﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using appMPSP.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appMPSP.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
        }

        public void ConsultarSivecCommand (object o , EventArgs e)
        {
            Navigation.PushAsync(new SivecPage());
        }


        public void ConsultarSielCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new SielPage());
        }

        public void ConsultarCagedCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new CagedPage());
        }

        public void ConsultarCadespCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new CadespPage());
        }
        public void ConsultarJucespCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new JucespPage());
        }

        public void ConsultarDetranCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new DetranPage());
        }

        public void ConsultarArispCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new ArispPage());
        }


        public void ConsultarArpenspCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new ArpenspPage());
        }


        public void ConsultarCensecCommand(object o, EventArgs e)
        {
            Navigation.PushAsync(new CensecPage());
        }




    }
}