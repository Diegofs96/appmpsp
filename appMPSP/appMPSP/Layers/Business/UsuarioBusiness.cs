﻿using System;
using System.Collections.Generic;
using System.Text;
using appMPSP.Layers.Service;
using appMPSP.Models;

namespace appMPSP.Layers.Business
{
    public class UsuarioBusiness
    {

        public Models.ConsultorModel Login(string email, string senha)
        {
            
            var _usuario = new UsuarioService().Login(new UsuarioModel(email.ToLower(), senha.ToLower()));


            var _consultor = new ConsultorModel();

            if (_usuario.IdUsuario != 0)
            {
                _consultor = new ConsultorService().Get(_usuario.IdUsuario);
            }

            return _consultor;

        }

    }
}
