﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using appMPSP.Layers.Service;
using System;


namespace appMPSP.Layers.Business
{
    class SielBusiness
    {

        public SielModel GetDadosSiel()
        {
            var _siel = new SielService().getDadosSiel();

            return _siel;
        }
    }
}
