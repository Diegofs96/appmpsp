﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using appMPSP.Layers.Service;
using System;

namespace appMPSP.Layers.Business
{
    class CadespBusiness
    {
        public CadespModel GetDadosCadesp()
        {
            var _cadesp = new CadespService().getDadosCadesp();

            return _cadesp;
        }
    }
}
