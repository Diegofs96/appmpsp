﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using appMPSP.Layers.Service;
using System;

namespace appMPSP.Layers.Business
{
    class SivecBusiness
    {

        public SivecModel GetDadosSivec()
        {
            var _sivec = new SivecService().getDadosSivec();

            return _sivec;
        }
    }
}
