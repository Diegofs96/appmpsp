﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using appMPSP.Layers.Service;
using System;


namespace appMPSP.Layers.Business
{
    class CagedBusiness
    {

        public CagedModel GetDadosCaged()
        {
            var _caged = new CagedService().getDadosCaged();

            return _caged;
        }
    }
}
