﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using appMPSP.Models;
using Newtonsoft.Json;

namespace appMPSP.Layers.Service
{
    class ConsultorService
    {
        public Models.ConsultorModel Get(int _id)
        {
            var uri = String.Format("http://fiapcoin-webapi.azurewebsites.net/api/Investidor/{0}", _id);

            System.Net.Http.HttpClient client = new HttpClient();
            var resposta = client.GetAsync(uri).Result;

            if (resposta.IsSuccessStatusCode)
            {
                var resultado = resposta.Content.ReadAsStringAsync().Result;
                var investidor = JsonConvert.DeserializeObject<ConsultorModel>(resultado);
                return investidor;
            }
            else
            {
                throw new Exception("Dados do investidor não encontrado!");
            }
        }

    }
}
