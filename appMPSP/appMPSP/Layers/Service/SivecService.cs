﻿using System;
using System.Net.Http;
using System.Text;
using appMPSP.Models;
using Newtonsoft.Json;

namespace appMPSP.Layers.Service
{
    class SivecService
    {
        public UsuarioModel UsuarioModel { get; set; }
        public SivecModel getDadosSivec()
        {

            var apiSivec = "https://92zvbr410j.execute-api.us-east-1.amazonaws.com/api/sivec";

            UsuarioModel _user = new UsuarioModel();

            _user.User = "Test Use";
            _user.PesquisaModel.Cpf = "1";

            var conteudoJson = Newtonsoft.Json.JsonConvert.SerializeObject(_user);
            var conteudoJsonString = new StringContent(conteudoJson, Encoding.UTF8, "application/json");

            System.Net.Http.HttpClient client = new HttpClient();
            var resposta = client.PostAsync(apiSivec, conteudoJsonString).Result;

            if (resposta.IsSuccessStatusCode)
            {
                var resultado = resposta.Content.ReadAsStringAsync().Result;
                var sivec = JsonConvert.DeserializeObject<SivecModel>(resultado);
                return sivec;
            }
            else
            {
                throw new Exception("Nenhum resultado encontrado");
            }

            
        }


    }
}
