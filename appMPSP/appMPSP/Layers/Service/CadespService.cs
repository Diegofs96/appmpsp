﻿using System;
using System.Net.Http;
using System.Text;
using appMPSP.Models;
using Newtonsoft.Json;

namespace appMPSP.Layers.Service
{
    class CadespService
    {
        public CadespModel getDadosCadesp()
        {

            var apiCadesp = "https://sxpmo6ztf1.execute-api.us-east-1.amazonaws.com/demo/cadesp";

            System.Net.Http.HttpClient client = new HttpClient();
            var resposta = client.GetAsync(apiCadesp).Result;

            if (resposta.IsSuccessStatusCode)
            {
                var resultado = resposta.Content.ReadAsStringAsync().Result;
                var cadesp = JsonConvert.DeserializeObject<CadespModel>(resultado);
                return cadesp;
            }
            else
            {
                throw new Exception("Nenhum resultado encontrado");
            }


        }
    }
}
