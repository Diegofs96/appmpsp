﻿using System;
using System.Net.Http;
using System.Text;
using appMPSP.Models;
using Newtonsoft.Json;
namespace appMPSP.Layers.Service
{
    class CagedService
    {

        public CagedModel getDadosCaged()
        {

            var apiCaged = "https://sxpmo6ztf1.execute-api.us-east-1.amazonaws.com/demo/caged";

            System.Net.Http.HttpClient client = new HttpClient();
            var resposta = client.GetAsync(apiCaged).Result;

            if (resposta.IsSuccessStatusCode)
            {
                var resultado = resposta.Content.ReadAsStringAsync().Result;
                var caged = JsonConvert.DeserializeObject<CagedModel>(resultado);
                return caged;
            }
            else
            {
                throw new Exception("Nenhum resultado encontrado");
            }


        }
    }
}
