﻿using System;
using System.Net.Http;
using System.Text;
using appMPSP.Models;
using Newtonsoft.Json;

namespace appMPSP.Layers.Service
{
    class SielService
    {

        public SielModel getDadosSiel()
        {

            var apiSiel = "https://sxpmo6ztf1.execute-api.us-east-1.amazonaws.com/demo/siel";

            System.Net.Http.HttpClient client = new HttpClient();
            var resposta = client.GetAsync(apiSiel).Result;

            if (resposta.IsSuccessStatusCode)
            {
                var resultado = resposta.Content.ReadAsStringAsync().Result;
                var siel = JsonConvert.DeserializeObject<SielModel>(resultado);
                return siel;
            }
            else
            {
                throw new Exception("Nenhum resultado encontrado");
            }


        }
    }
}
