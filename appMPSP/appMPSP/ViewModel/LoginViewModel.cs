﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using System;

namespace appMPSP.ViewModel
{
    public class LoginViewModel
    {

        public ICommand EntrarClickedCommand { get; private set; }

        public UsuarioModel Usuario { get; set; }

        public LoginViewModel()
        {
            Usuario = new UsuarioModel();
            EntrarClickedCommand = new Command(() => {
                try
                {
                    // Efetuando o login
                    var usuarioBusiness = new Layers.Business.UsuarioBusiness();
                    var consultor = usuarioBusiness.Login(Usuario.Email, Usuario.Senha);

                    if (consultor.IdUsuario != 0)
                    {
                        MessagingCenter.Send<LoginViewModel>(this, "LoginSucesso");
                    }
                    else
                    {
                        App.MensagemAlerta("Login ou senha inválida");
                    }
                }
                catch (Exception ex)
                {
                    App.MensagemAlerta("Login ou senha inválida. Detalhe: " + ex.Message);
                }
            });
        }
    }
}
