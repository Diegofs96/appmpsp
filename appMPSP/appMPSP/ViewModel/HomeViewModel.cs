﻿using System.Windows.Input;
using Xamarin.Forms;
using appMPSP.Models;
using appMPSP.Layers.Business;
using System;

namespace appMPSP.ViewModel
{
    class HomeViewModel
    {
        public ICommand ConsultarSivecCommand { get; private set; }
        public ICommand ConsultarSielCommand { get; private set; }
        public ICommand ConsultarCadespCommand { get; private set; }
        public ICommand ConsultarCagedCommand { get; private set; }

        public SivecModel SivecModel { get; set; }

        public HomeViewModel()
        {
            SivecModel = new SivecModel();

            ConsultarSivecCommand = new Command(() => {
                try
                {
                    var sivecBusiness = new SivecBusiness();
                    var sivec = sivecBusiness.GetDadosSivec();

                    if (sivec.Id_pesquisado != 0)
                    {
                        MessagingCenter.Send<HomeViewModel>(this, "DadosSivec");
                    }
                    else
                    {
                        App.MensagemAlerta("Nenhum dado foi encontrado");
                    }
                }
                catch (Exception ex)
                {
                    App.MensagemAlerta("Nenhum dado foi encontrado. Detalhe: " + ex.Message);
                }
            });

            ConsultarSielCommand = new Command(() => {
                try
                {
                    var sielBusiness = new SielBusiness();
                    var siel = sielBusiness.GetDadosSiel();

                    if (siel.Id_pesquisado != 0)
                    {
                        MessagingCenter.Send<HomeViewModel>(this, "DadosSiel");
                    }
                    else
                    {
                        App.MensagemAlerta("Nenhum dado foi encontrado");
                    }
                }
                catch (Exception ex)
                {
                    App.MensagemAlerta("Nenhum dado foi encontrado. Detalhe: " + ex.Message);
                }
            });

            ConsultarCadespCommand = new Command(() => {
                try
                {
                    var cadespBusiness = new CadespBusiness();
                    var cadesp = cadespBusiness.GetDadosCadesp();

                    if (cadesp.Id_pesquisado != 0)
                    {
                        MessagingCenter.Send<HomeViewModel>(this, "DadosCadesp");
                    }
                    else
                    {
                        App.MensagemAlerta("Nenhum dado foi encontrado");
                    }
                }
                catch (Exception ex)
                {
                    App.MensagemAlerta("Nenhum dado foi encontrado. Detalhe: " + ex.Message);
                }
            });

            ConsultarCagedCommand = new Command(() => {
                try
                {
                    var cagedBusiness = new CagedBusiness();
                    var caged = cagedBusiness.GetDadosCaged();

                    if (caged.Id_pesquisado != 0)
                    {
                        MessagingCenter.Send<HomeViewModel>(this, "DadosCaged");
                    }
                    else
                    {
                        App.MensagemAlerta("Nenhum dado foi encontrado");
                    }
                }
                catch (Exception ex)
                {
                    App.MensagemAlerta("Nenhum dado foi encontrado. Detalhe: " + ex.Message);
                }
            });
        }
    }
}
