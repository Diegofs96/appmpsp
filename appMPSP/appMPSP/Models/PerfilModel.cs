﻿using System;
using System.Collections.Generic;
using System.Text;

namespace appMPSP.Models
{
    public class PerfilModel
    {
        public PerfilModel()
        {
        }

        public PerfilModel(int Id, String Nome)
        {
            this.IdPerfil = Id;
            this.NomePerfil = Nome;
        }

        public int IdPerfil
        {
            get;
            set;
        }

        public String NomePerfil
        {
            get;
            set;
        }
    }
}
