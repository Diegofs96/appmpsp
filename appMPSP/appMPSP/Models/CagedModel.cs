﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace appMPSP.Models
{
    class CagedModel
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private int id_pesquisado;
        public int Id_pesquisado
        {
            get { return id_pesquisado; }
            set
            {
                if (id_pesquisado != value)
                {
                    id_pesquisado = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String sistema;
        public String Sistema
        {
            get { return sistema; }
            set
            {
                if (sistema != value)
                {
                    sistema = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String nome;
        public String Nome
        {
            get { return nome; }
            set
            {
                if (nome != value)
                {
                    nome = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String basePis;
        public String BasePis
        {
            get { return basePis; }
            set
            {
                if (basePis != value)
                {
                    basePis = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String situacao;
        public String Situacao
        {
            get { return situacao; }
            set
            {
                if (situacao != value)
                {
                    basePis = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String cpf;
        public String Cpf
        {
            get { return cpf; }
            set
            {
                if (cpf != value)
                {
                    cpf = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String dataNascimento;
        public String DataNascimento
        {
            get { return dataNascimento; }
            set
            {
                if (dataNascimento != value)
                {
                    dataNascimento = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String ctpsSerie;
        public String CtpsSerie
        {
            get { return ctpsSerie; }
            set
            {
                if (ctpsSerie != value)
                {
                    ctpsSerie = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String sexo;
        public String Sexo
        {
            get { return sexo; }
            set
            {
                if (sexo != value)
                {
                    sexo = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String nacionalidadeNum;
        public String NacionalidadeNum
        {
            get { return nacionalidadeNum; }
            set
            {
                if (nacionalidadeNum != value)
                {
                    nacionalidadeNum = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String nacionalidadeText;
        public String NacionalidadeText
        {
            get { return nacionalidadeText; }
            set
            {
                if (nacionalidadeText != value)
                {
                    nacionalidadeText = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String racaNum;
        public String RacaNum
        {
            get { return racaNum; }
            set
            {
                if (racaNum != value)
                {
                    racaNum = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String racaText;
        public String RacaText
        {
            get { return racaText; }
            set
            {
                if (racaText != value)
                {
                    racaText = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String grauInstrucaoNum;
        public String GrauInstrucaoNum
        {
            get { return grauInstrucaoNum; }
            set
            {
                if (grauInstrucaoNum != value)
                {
                    grauInstrucaoNum = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String grauInstrucaoText;
        public String GrauInstrucaoText
        {
            get { return grauInstrucaoText; }
            set
            {
                if (grauInstrucaoText != value)
                {
                    grauInstrucaoText = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String deficiencia;
        public String Deficiencia
        {
            get { return deficiencia; }
            set
            {
                if (deficiencia != value)
                {
                    deficiencia = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String cep;
        public String Cep
        {
            get { return cep; }
            set
            {
                if (cep != value)
                {
                    cep = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String caged;
        public String Caged
        {
            get { return caged; }
            set
            {
                if (caged != value)
                {
                    caged = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String rais;
        public String Rais
        {
            get { return rais; }
            set
            {
                if (rais != value)
                {
                    rais = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String empresaCnpj;
        public String EmpresaCnpj
        {
            get { return empresaCnpj; }
            set
            {
                if (empresaCnpj != value)
                {
                    empresaCnpj = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String empresaRazaoSocial;
        public String EmpresaRazaoSocial
        {
            get { return empresaRazaoSocial; }
            set
            {
                if (empresaRazaoSocial != value)
                {
                    empresaRazaoSocial = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String empresaAtivEconomic;
        public String EmpresaAtivEconomic
        {
            get { return empresaAtivEconomic; }
            set
            {
                if (empresaAtivEconomic != value)
                {
                    empresaAtivEconomic = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String empresaAtivEconomicNum;
        public String EmpresaAtivEconomicNum
        {
            get { return empresaAtivEconomicNum; }
            set
            {
                if (empresaAtivEconomicNum != value)
                {
                    empresaAtivEconomicNum = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String empresaAtivEconomicTipo;
        public String EmpresaAtivEconomicTipo
        {
            get { return empresaAtivEconomicTipo; }
            set
            {
                if (empresaAtivEconomicTipo != value)
                {
                    empresaAtivEconomicTipo = value;
                    NotifyPropertyChanged();
                }
            }

        }






        public String empresaNumFilial;
        public String EmpresaNumFilial
        {
            get { return empresaNumFilial; }
            set
            {
                if (empresaNumFilial != value)
                {
                    empresaNumFilial = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String totalVinculos;
        public String TotalVinculos
        {
            get { return totalVinculos; }
            set
            {
                if (totalVinculos != value)
                {
                    totalVinculos = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String umDia;
        public String UmDia
        {
            get { return umDia; }
            set
            {
                if (umDia != value)
                {
                    umDia = value;
                    NotifyPropertyChanged();
                }
            }

        }


        public String admissoes;
        public String Admissoes
        {
            get { return admissoes; }
            set
            {
                if (admissoes != value)
                {
                    admissoes = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String desligamentos;
        public String Desligamentos
        {
            get { return desligamentos; }
            set
            {
                if (desligamentos != value)
                {
                    desligamentos = value;
                    NotifyPropertyChanged();
                }
            }

        }





        public String ultimoDia;
        public String UltimoDia
        {
            get { return ultimoDia; }
            set
            {
                if (ultimoDia != value)
                {
                    desligamentos = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String variacaoAbsoluta;
        public String VariacaoAbsoluta
        {
            get { return variacaoAbsoluta; }
            set
            {
                if (variacaoAbsoluta != value)
                {
                    variacaoAbsoluta = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprEmpresaRazao;
        public String AutorizadprEmpresaRazao
        {
            get { return autorizadprEmpresaRazao; }
            set
            {
                if (autorizadprEmpresaRazao != value)
                {
                    autorizadprEmpresaRazao = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprEmpresaCnpj;
        public String AutorizadprEmpresaCnpj
        {
            get { return autorizadprEmpresaCnpj; }
            set
            {
                if (autorizadprEmpresaCnpj != value)
                {
                    autorizadprEmpresaCnpj = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String autorizadprLogradouro;
        public String AutorizadprLogradouro
        {
            get { return autorizadprLogradouro; }
            set
            {
                if (autorizadprLogradouro != value)
                {
                    autorizadprLogradouro = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String autorizadprBairro;
        public String AutorizadprBairro
        {
            get { return autorizadprBairro; }
            set
            {
                if (autorizadprBairro != value)
                {
                    autorizadprBairro = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprMunicipioNum;
        public String AutorizadprMunicipioNum
        {
            get { return autorizadprMunicipioNum; }
            set
            {
                if (autorizadprMunicipioNum != value)
                {
                    autorizadprMunicipioNum = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprMunicipioNome;
        public String AutorizadprMunicipioNome
        {
            get { return autorizadprMunicipioNome; }
            set
            {
                if (autorizadprMunicipioNome != value)
                {
                    autorizadprMunicipioNome = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String autorizadprUf;
        public String AutorizadprUf
        {
            get { return autorizadprUf; }
            set
            {
                if (autorizadprUf != value)
                {
                    autorizadprUf = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprCep;
        public String AutorizadprCep
        {
            get { return autorizadprCep; }
            set
            {
                if (autorizadprCep != value)
                {
                    autorizadprCep = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprNome;
        public String AutorizadprNome
        {
            get { return autorizadprNome; }
            set
            {
                if (autorizadprNome != value)
                {
                    autorizadprCep = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprCpf;
        public String AutorizadprCpf
        {
            get { return autorizadprCpf; }
            set
            {
                if (autorizadprCpf != value)
                {
                    autorizadprCpf = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprTelefoneDDD;
        public String AutorizadprTelefoneDDD
        {
            get { return autorizadprTelefoneDDD; }
            set
            {
                if (autorizadprTelefoneDDD != value)
                {
                    autorizadprTelefoneDDD = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String autorizadprTelefoneNum;
        public String AutorizadprTelefoneNum
        {
            get { return autorizadprTelefoneNum; }
            set
            {
                if (autorizadprTelefoneNum != value)
                {
                    autorizadprTelefoneNum = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String autorizadprTelefoneRamal;
        public String AutorizadprTelefoneRamal
        {
            get { return autorizadprTelefoneRamal; }
            set
            {
                if (autorizadprTelefoneRamal != value)
                {
                    autorizadprTelefoneRamal = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String autorizadprTelefoneEmail;
        public String AutorizadprTelefoneEmail
        {
            get { return autorizadprTelefoneEmail; }
            set
            {
                if (autorizadprTelefoneEmail != value)
                {
                    autorizadprTelefoneEmail = value;
                    NotifyPropertyChanged();
                }
            }

        }
    }
}

