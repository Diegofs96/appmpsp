﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace appMPSP.Models
{
    class SielModel
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int id_pesquisado;
        public int Id_pesquisado
        {
            get { return id_pesquisado; }
            set
            {
                if (id_pesquisado != value)
                {
                    id_pesquisado = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String sistema;
        public String Sistema
        {
            get { return sistema; }
            set
            {
                if (sistema != value)
                {
                    sistema = value;
                    NotifyPropertyChanged();
                }
            }

        }



        public String nome;
        public String Nome
        {
            get { return nome; }
            set
            {
                if (nome != value)
                {
                    nome = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String tituloeleitor;
        public String Tituloeleitor
        {
            get { return tituloeleitor; }
            set
            {
                if (tituloeleitor != value)
                {
                    tituloeleitor = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String nascimento;
        public String Nascimento
        {
            get { return nascimento; }
            set
            {
                if (nascimento != value)
                {
                    nascimento = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public int zona;
        public int Zona
        {
            get { return zona; }
            set
            {
                if (zona != value)
                {
                    zona = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String endresidencial;
        public String Endtrabalho
        {
            get { return endresidencial; }
            set
            {
                if (endresidencial != value)
                {
                    endresidencial = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String municipio;
        public String Municipio
        {
            get { return municipio; }
            set
            {
                if (municipio != value)
                {
                    municipio = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String uf;
        public String Uf
        {
            get { return uf; }
            set
            {
                if (uf != value)
                {
                    uf = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String naturalidade;
        public String Naturalidade
        {
            get { return naturalidade; }
            set
            {
                if (naturalidade != value)
                {
                    naturalidade = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String datadomicilio;
        public String Datadomicilio
        {
            get { return datadomicilio; }
            set
            {
                if (datadomicilio != value)
                {
                    datadomicilio = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String nomePai;
        public String NomePai
        {
            get { return nomePai; }
            set
            {
                if (nomePai != value)
                {
                    nomePai = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String nomeMae;
        public String NomeMae
        {
            get { return nomeMae; }
            set
            {
                if (nomeMae != value)
                {
                    nomeMae = value;
                    NotifyPropertyChanged();
                }
            }
        }





        public String codvalidacao;
        public String Codvalidacao
        {
            get { return codvalidacao; }
            set
            {
                if (codvalidacao != value)
                {
                    codvalidacao = value;
                    NotifyPropertyChanged();
                }
            }
        }


    }
}
