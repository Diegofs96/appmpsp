﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace appMPSP.Models
{
    class SivecModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int id_pesquisado;

        public int Id_pesquisado
        {
            get { return id_pesquisado; }
            set
            {
                if (id_pesquisado != value)
                {
                    id_pesquisado = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public String sistema;

        public String Sistema
        {
            get { return sistema; }
            set
            {
                if (sistema != value)
                {
                    sistema = value;
                    NotifyPropertyChanged();
                }
            }

        }


        public String nome;
        public String Nome
        {
            get { return nome; }
            set
            {
                if (nome != value)
                {
                    nome = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String sexo;
        public String Sexo
        {
            get { return sexo; }
            set
            {
                if (sexo != value)
                {
                    sexo = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String nascimento;
        public String Nascimento
        {
            get { return nascimento; }
            set
            {
                if (nascimento != value)
                {
                    nascimento = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String rgPessoa;
        public String RgPessoa
        {
            get { return rgPessoa; }
            set
            {
                if (rgPessoa != value)
                {
                    rgPessoa = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String nrovec;
        public String Nrovec
        {
            get { return nrovec; }
            set
            {
                if (nrovec != value)
                {
                    nrovec = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String tiporg;
        public String Tiporg
        {
            get { return tiporg; }
            set
            {
                if (tiporg != value)
                {
                    tiporg = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String datarg;
        public String Datarg
        {
            get { return Datarg; }
            set
            {
                if (datarg != value)
                {
                    datarg = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String alcunha;
        public String Alcunha
        {
            get { return alcunha; }
            set
            {
                if (alcunha != value)
                {
                    alcunha = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String civil;
        public String Civil
        {
            get { return civil; }
            set
            {
                if (civil != value)
                {
                    civil = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String endresidencial;
        public String Endresidencial
        {
            get { return endresidencial; }
            set
            {
                if (endresidencial != value)
                {
                    endresidencial = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String endtrabalho;
        public String Endtrabalho
        {
            get { return endtrabalho; }
            set
            {
                if (endtrabalho != value)
                {
                    endtrabalho = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String naturalidade;
        public String Naturalidade
        {
            get { return naturalidade; }
            set
            {
                if (naturalidade != value)
                {
                    naturalidade = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String naturalizado;
        public String Naturalizado
        {
            get { return naturalizado; }
            set
            {
                if (naturalizado != value)
                {
                    naturalizado = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String profissao;
        public String Profissao
        {
            get { return profissao; }
            set
            {
                if (profissao != value)
                {
                    profissao = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String olhos;
        public String Olhos
        {
            get { return olhos; }
            set
            {
                if (olhos != value)
                {
                    olhos = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public String corcabelo;
        public String Corcabelo
        {
            get { return corcabelo; }
            set
            {
                if (corcabelo != value)
                {
                    corcabelo = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String corpele;
        public String Corpele
        {
            get { return corpele; }
            set
            {
                if (corpele != value)
                {
                    corpele = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String nomePai;
        public String NomePai
        {
            get { return nomePai; }
            set
            {
                if (nomePai != value)
                {
                    nomePai = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public String nomeMae;
        public String NomeMae
        {
            get { return nomeMae; }
            set
            {
                if (nomeMae != value)
                {
                    nomeMae = value;
                    NotifyPropertyChanged();
                }
            }
        }


    }
}
