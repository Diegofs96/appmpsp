﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace appMPSP.Models
{
    class CadespModel
    {

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }





        public String ie;
        public String Ie
        {
            get { return ie; }
            set
            {
                if (ie != value)
                {
                    ie = value;
                    NotifyPropertyChanged();
                }
            }

        }




        private int id_pesquisado;
        public int Id_pesquisado
        {
            get { return id_pesquisado; }
            set
            {
                if (id_pesquisado != value)
                {
                    id_pesquisado = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String sistema;
        public String Sistema
        {
            get { return sistema; }
            set
            {
                if (sistema != value)
                {
                    sistema = value;
                    NotifyPropertyChanged();
                }
            }

        }




        public String cnpj;
        public String Cnpj
        {
            get { return cnpj; }
            set
            {
                if (cnpj != value)
                {
                    cnpj = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String nomeEmpresarial;
        public String NomeEmpresarial
        {
            get { return nomeEmpresarial; }
            set
            {
                if (nomeEmpresarial != value)
                {
                    nomeEmpresarial = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String drt;
        public String Drt
        {
            get { return drt; }
            set
            {
                if (drt != value)
                {
                    drt = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String situacao;
        public String Situacao
        {
            get { return situacao; }
            set
            {
                if (situacao != value)
                {
                    situacao = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String dataInscricaoEstado;
        public String DataInscricaoEstado
        {
            get { return dataInscricaoEstado; }
            set
            {
                if (dataInscricaoEstado != value)
                {
                    dataInscricaoEstado = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String regimeEstadual;
        public String RegimeEstadual
        {
            get { return regimeEstadual; }
            set
            {
                if (regimeEstadual != value)
                {
                    regimeEstadual = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String pontoFiscal;
        public String PontoFiscal
        {
            get { return pontoFiscal; }
            set
            {
                if (pontoFiscal != value)
                {
                    pontoFiscal = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String nire;
        public String Nire
        {
            get { return nire; }
            set
            {
                if (nire != value)
                {
                    nire = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String ocorrenciaFiscal;
        public String OcorrenciaFiscal
        {
            get { return ocorrenciaFiscal; }
            set
            {
                if (ocorrenciaFiscal != value)
                {
                    ocorrenciaFiscal = value;
                    NotifyPropertyChanged();
                }
            }
        }



        public String tipoUnidade;
        public String TipoUnidade
        {
            get { return tipoUnidade; }
            set
            {
                if (tipoUnidade != value)
                {
                    tipoUnidade = value;
                    NotifyPropertyChanged();
                }
            }
        }




        public String formaAtuacao;
        public String FormaAtuacao
        {
            get { return formaAtuacao; }
            set
            {
                if (formaAtuacao != value)
                {
                    formaAtuacao = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
