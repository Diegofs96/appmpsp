﻿using System;
using System.Collections.Generic;
using System.Text;

namespace appMPSP.Models
{
    public class ConsultorModel
    {
        public int IdUsuario
        {
            get;
            set;
        }

        public String NomeConsultor
        {
            get;
            set;
        }

        public String EmailConsultor
        {
            get;
            set;
        }

        public String TelefoneConsultor
        {
            get;
            set;
        }

        public PerfilModel PerfilConsultor
        {
            get;
            set;
        }


        public ConsultorModel()
        {
        }

        public ConsultorModel(int _id)
        {
            this.IdUsuario = _id;
        }

        public ConsultorModel(int Id, String Nome, String Email, String Fone, double Valor, PerfilModel Perfil)
        {
            this.IdUsuario = Id;
            this.NomeConsultor = Nome;
            this.EmailConsultor = Email;
            this.TelefoneConsultor = Fone;
            this.PerfilConsultor = Perfil;
        }
    }
}
