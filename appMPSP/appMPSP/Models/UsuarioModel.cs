﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace appMPSP.Models
{
    public class UsuarioModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int idUsuario;
        public int IdUsuario
        {
            get { return idUsuario; }
            set
            {
                if (idUsuario != value)
                {
                    idUsuario = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private String email;
        public String Email
        {
            get { return email; }
            set
            {
                if (email != value)
                {
                    email = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private String senha;
        public String Senha
        {
            get { return senha; }
            set
            {
                if (senha != value)
                {
                    senha = value;
                    NotifyPropertyChanged();
                }
            }

        }

        public String user;
        public String User
        {
            get { return user; }
            set
            {
                if (user != value)
                {
                    user = value;
                    NotifyPropertyChanged();
                }
            }

        }

        public String cpf;
        public String Cpf
        {
            get { return cpf; }
            set
            {
                if (cpf != value)
                {
                    cpf = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private PesquisaModel pesquisaModel;
        public PesquisaModel PesquisaModel
        {
            get { return pesquisaModel; }
            set
            {
                if (pesquisaModel != value)
                {
                    pesquisaModel = value;
                    NotifyPropertyChanged();
                }
            }
        }







        public UsuarioModel()
        {

        }

        public UsuarioModel(string _email, string _senha)
        {
            this.Email = _email;
            this.Senha = _senha;
        }

    }
}
